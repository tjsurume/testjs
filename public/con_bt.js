
const MIDI_SERVICE_UUID = '03b80e5a-ede8-4b33-a751-6ce34ec4c700';
const MIDI_CHARACTERISITIC_UUID = '7772e5db-3868-4112-a1a9-f2669d106bf3';

function disconnect() {
   if (!midi_device || !midi_device.gatt.connected) return ;
   midi_device.gatt.disconnect();
   alert("Disconnect BLE MIDI")
 }

function connect() { 
   navigator.bluetooth.requestDevice({
     filters: [{
       services: [MIDI_SERVICE_UUID],
     }],
   })
   .then(device => {
     midi_device = device;
     console.log("device", device);
     return device.gatt.connect();
   })
   .then(server =>{
     console.log("server", server)
     return server.getPrimaryService(MIDI_SERVICE_UUID);
   })
   .then(service => {
     console.log("service", service)
     return service.getCharacteristic(MIDI_CHARACTERISITIC_UUID)
   })
   .then(chara => {
     console.log("MIDI_CHARACTERISITIC:", chara)
     alert("Connect BLE MIDI");
     characteristic = chara;
     characteristic.startNotifications();
     characteristic.addEventListener('characteristicvaluechanged',onMIDIEvent); 
   }) 
   .catch(error => {
     alert("Failed to connect BLE MIDI.");
     console.log(error);
   });   
 }

 function append_str(retstr){
    past_str =  "<br>" + document.getElementById('logbox').innerHTML;
    
    if(past_str.length > 1000)past_str = "";
    document.getElementById('logbox').innerHTML = retstr + past_str;

 }

 function onMIDIEvent (event) {
   var ret = event.target.value;
   var retstr = "";
   for(var i  = 0; i < ret.buffer.byteLength ;i++){
     retstr += ret.getUint8(i) + " ";
   }

   append_str(retstr);
}


 function send_midi(val0, val1, val2){
  var array_u8 = new Uint8Array(5);
  ts_hi = 0x80; // unconsidered timestamp
  ts_lo = 0x80; // unconsidered timestamp
 
  array_u8[0] = 0x80;
  array_u8[1] = 0x80;
  array_u8[2] = val0;
  array_u8[3] = val1;
  array_u8[4] = val2;

  append_str(array_u8);
  if(typeof characteristic != 'undefined') characteristic.writeValue(array_u8);

 }


 function test_note(value){
  send_midi(0x90, 0x45, value ? 0x7F: 0x00);
 }


function init(){
 var sliders = document.querySelectorAll('input[type=range]');
 for(var i = 0; i < sliders.length; i++){
   console.log(i);
   sliders[i].addEventListener("input", function(e){
    
     console.log(e.target.id, e.target.value);
     midi_id = 0;
     switch(e.target.id){
       case "slider1": midi_id = 0x01; break;
       case "slider2": midi_id = 0x02; break;
       case "slider3": midi_id = 0x11; break;
     }
     send_midi(0xB0, midi_id, Number(e.target.value))
   })
 }


 var checkbox = document.getElementById("test_note");
 checkbox.addEventListener("change", function(e){
  test_note(e.target.checked);
 })


}

window.onload = function() {
 init();
}

